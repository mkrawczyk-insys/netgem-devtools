const watch = require("watch");
const { readFileSync } = require("fs");
const { join } = require("path");
const ip = require('ip');

const IP = ip.address();
const PORT = process.env.PORT || 8071;

const INDEX_PATH = join(process.cwd(), "index.html");
const indexContent = readFileSync(INDEX_PATH, { encoding: 'utf8' });
const TITLE = ((indexContent.match(/<title[^>]*>([^<]+)<\/title>/) || [])[1] || "Test App").trim();
console.log(`Running ${TITLE}...`);


/***********************************************************************************/
/*  HTTP external server                                                           */

console.log("Starting http server...");
const connect = require('connect');
const serveStatic = require('serve-static');
const server = connect()
.use(serveStatic(process.cwd(), {
    setHeaders: function(res){
        res.setHeader("Access-Control-Allow-Origin", "*");
        res.setHeader('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
        res.setHeader('Access-Control-Allow-Headers', 'Content-Type');
    }
}))
.listen(PORT, IP, function(){
    console.log(`Server running on ${ip.address()}:${PORT}...`);
});


/***********************************************************************************/
/*  WebSocket internal server                                                      */

const Server = require("simple-websocket/server");
const ws = new Server({ port: 8072 });

ws.on('connection', (socket) => {
    socket.write(JSON.stringify({ command: "getinfo", ip: IP, port: PORT, name: TITLE }));
    /* socket.on('data', function (data) {
        console.log('data', data);
    }) */
    socket.on('close', function () {})
    socket.on('error', function (err) {})
});


/***********************************************************************************/
/*  React website                                                                  */

const { exec } = require('child_process');
const reactScripts = exec(
    `cross-env REACT_APP_DEVICE_IP=${process.env.deviceIp || ""} REACT_APP_DEVICE_PORT=${process.env.devicePort || ""} yarn start`,
    {
        cwd: __dirname,
        env: {
            ...process.env,
            REACT_APP_DEVICE_IP: process.env.deviceIp,
            REACT_APP_DEVICE_PORT: process.env.devicePort
        }
    });
reactScripts.stdout.on('data', (data) => {
    // console.log(data.toString());
  });
  
reactScripts.stderr.on('data', (data) => {
    // console.log(data.toString());
});

reactScripts.on('exit', (code) => {
    console.log(`Child exited with code ${code}`);
});