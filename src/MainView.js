import React, { Component } from 'react';
import './MainView.css';
import DeviceScreen from './DeviceScreen';
import Console from './Console';
import PropTypes from 'prop-types';

class MainView extends Component {
  static contextTypes = {
    appData: PropTypes.object,
    stb: PropTypes.object
  };


  render() {
    const { appData, stb } = this.context;

    return (
      <div className="MainView">
          <div className="MainView__Bar">
              <button className="Bar__item">{stb.appId || appData.name}</button>
          </div>
          <div className="MainView__Screen">
            <DeviceScreen />
          </div>
          <div className="MainView__Console">
            <Console />
          </div>
      </div>
    );
  }
}

export default MainView;