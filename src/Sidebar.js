import React, { Component } from 'react';
import PropTypes from 'prop-types';
import "./Sidebar.css";
import classNames from "classnames";
import SidebarElementsTab from "./SidebarElementsTab";

class Sidebar extends Component {
  static contextTypes = {
    stb: PropTypes.object,
    appData: PropTypes.object
  };

  state = {
      activeTab: "Elements"
  }

  renderElementsTab = () => {
    return <SidebarElementsTab />;
  }

  renderNetworkTab = () => {
    return "Network";
  }

  renderApplicationTab = () => {
    return "Application";
  }


  render() {
    const tabs = {
        Elements: this.renderElementsTab,
        Network: this.renderNetworkTab,
        Application: this.renderApplicationTab
    }

    const { activeTab } = this.state;
    return (
        <div className="Sidebar">
            <div className="Sidebar__toolbar">
                <button className="Toolbar__button">icon</button>
                { Object.keys(tabs).map(key => (
                    <button
                        key={key}
                        className={classNames("Toolbar__button", activeTab === key && "Toolbar__button--active")}
                        onClick={() => this.setState({ activeTab: key })}
                    >
                        {key}
                    </button>
                )) }
            </div>
            <div className="Sidebar__content">
                { tabs[activeTab]() }
            </div>
        </div>
    );
  }
}

export default Sidebar;
