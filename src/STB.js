import defaults from "lodash/defaults";
import { objectToQueryString, parseStbLogs, parseStbCommandLog, decodeServerJson } from "./utils";
import { dumpValue } from "./utils/dump";

export default class STB  {

    deviceIp = null;
    devicePort = null;
    appId = null;

    constructor(deviceIp, devicePort = "80") {
        // super();
        this.deviceIp = deviceIp.toString().trim();
        this.devicePort = devicePort.toString().trim();

        if (!this.deviceIp) {
            throw new Error("Device IP is not valid");
        }
    }

    get url() {
        return `http://${this.deviceIp}:${this.devicePort}`;
    }

    setAppId(appId, appZap) {
        this.appId = appId;
        this.appZap = appZap;
    }

    async send(command, requestConfig = {}) {
        const config = defaults(requestConfig, {
            method: 'GET',
            args: null,
            urlEncoded: true,
            bind: null,
            onInvalid: function(errorText) {},
            onSend: function(config, xhr) {},
            onSuccess: function(responseText, responseXml, config, xhr) {},
            onFailure: function(config, xhr) {},
            body: null,
            ip: this.ip,
            port: this.port,
            command
        });
        
        let url = `${this.url}/${command}`;

        if (config.params) {
            if (typeof config.params === 'string') url += '?' + config.params;
            else url += '?' + objectToQueryString(config.params);
        }

        // request
        var req = new Request(url, {
            method: config.method,
            urlEncoded: config.urlEncoded,
            noCache: true,
            body: config.body,
            onSuccess: (responseText, responseXml) => {
                // console.log('[SUCCESS] ' + url, responseText, responseXml);
                // config.onSuccess.call(config.bind, responseText, responseXml, config, req);
            },
            onFailure: function(xhr) {
                // console.log('[FAILURE] ' + url);
                // config.onFailure.call(config.bind, config, req);
            }
        });

        // @critical
        // bug fix on xhr to allow cross domain!!!
        delete req.headers['X-Requested-With'];
        
        //  @critical
        // fix webkit
        if (config.method === 'POST') { // ['safari', 'chrome'].contains(Browser.name) &&
            req.headers['Content-Type'] = 'text/plain';
        }
    
        // content type
        if (config.contentType) {
            req.headers['Content-Type'] = config.contentType;
        }
    
        try {
            // console.log('[SEND] ' + url);
            config.onSend.call(config.bind, config, req);
            
            // @critical
            // chrome throws an error like: Uncaught Error: NETWORK_ERR: XMLHttpRequest Exception 101
            // req.send(config.data);
            const response = await fetch(req);
            return response;
        } catch (e) {
            // console.log('[SEND FAILURE] ' + url, e);
        }
    };

    getScreenUrl = (width, height, noVideo = 0) =>
        `${this.url}/Debug/Command/screenDump?width=${width}&height=${height}&noVideo=${Number(noVideo)}&${new Date().getTime()}`;

    ping = () => this.send('Debug/State/ping');

    injectJS = body => this.send('Debug/Channel/inject', {
        method: "POST",
        urlEncoded: false,
        params: {
            id: this.appId,
            zap: this.appZap,
            resetLogs: 1
        },
        body
    });
    
    runJS = async code => {
        try {
            const injectedScript = `throw new Error("[OUTPUT]"+JSON.stringify(${code}))`
            const response = await this.send('Debug/Channel/inject', {
                method: "POST",
                urlEncoded: false,
                params: {
                    id: this.appId,
                    zap: this.appZap,
                    resetLogs: 1
                },
                body: injectedScript
            });

            const { logs } = decodeServerJson(await response.text());
            const { error } = parseStbCommandLog(logs);
            if (error.startsWith("Uncaught Error: [OUTPUT]")) {
                return JSON.parse(error.replace("Uncaught Error: [OUTPUT]", ""));
            }
        } catch (ex) {
            
    }
    }

    runOverlayApp = (name, url, template = 'app.html?keepbg=0&keeptitle=0') => this.send('Debug/Channel/overlay', {
        params: {
            name,
            url,
            template
        }
    });
    
    sendKeyboardKey = (keyCode, charCode) => this.send('RemoteControl/KeyHandling/sendKBKey', {
        params: {
            keyCode,
            charCode
        }
    });

    sendKey = (key, avoidLongPress = 1) => this.send('RemoteControl/KeyHandling/sendKey', {
        params: {
            key,
            avoidLongPress
        }
    });

    getJavaScriptLogs = async () => {
        const response = await this.send('Debug/Log/get', {
            params: {
                name: 'javascript'
            }
        });

        return parseStbLogs(await response.text());
    }

    resetJavaScriptLogs = () => this.send('Debug/Log/reset', {
        params: {
            name: 'javascript'
        }
    });
    
    reloadApp = () => this.send('Debug/Channel/reload', {
        params: {
            id: this.appId,
        }
    });

    dumpObject = async (body, depth = 2, returnRaw) => {
        const response = await this.send('Debug/Channel/dumpJSObject', {
            method: "POST",
            urlEncoded: false,
            params: {
                id: this.appId,
                depth
            },
            body
        });

        const rawJson = decodeServerJson(await response.text());
        return returnRaw ? rawJson : dumpValue(rawJson);
    }
}