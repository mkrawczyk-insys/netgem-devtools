import React, { Component } from 'react';
import PropTypes from 'prop-types';
import "./ThemaVisualiser.css";
import set from "lodash/set";
import get from "lodash/get";
import merge from "lodash/merge";
import { decodeServerJson } from './utils';

const GET_WIDGETS_VISIBILITY = `
(function(widgets) {
    return Object.keys(widgets).sort().map(function(key) {
        return widgets[key].stateGet() === "enter" ? 1 : 0;
    }).join("")
})(NGM.application["mainForm"]["widgets"])`;

const REFRESH_WIDGETS_TIMEOUT = 3000;
const ROOT_PATH = "NGM.application.mainForm";

class WidgetsManager extends Component {
  static contextTypes = {
    stb: PropTypes.object,
    appData: PropTypes.object
  };

  static childContextTypes = {
    widgets: PropTypes.object
  };

  jsonFile = null;
  baseWidgets = {};
  activeWidgets = {};
  state = {
      visibility: null
  };

  widgets = new EventTarget();

  getChildContext() {
    const { widgets } = this;

    widgets.changeActiveElement = this.changeActiveElement;
    return { widgets };
  }

  changeActiveElement = (key) => {
    this.widgets.dispatchEvent(new CustomEvent("changedActiveElement", { detail: key }));
  }

  componentDidMount() {
    const { dumpObject, url } = this.context.stb;
    const { ip, port } = this.context.appData;

    setTimeout(async () => {
      try {
        const jsonFileName = await dumpObject(`${ROOT_PATH}._formDefinition.jsonFile`, 1);
        // this.jsonWidgets = await dumpObject(`${ROOT_PATH}._formDefinition.jsonData.widgets`, 7);

        const jsonWidgetsFull = await fetch(`http://${ip}:${port}/${jsonFileName}`, {
            method: 'GET',
            cache: 'no-cache',
            headers: {
                'Content-Type': 'text/plain'
            }
        });

        this.baseWidgets = decodeServerJson(await jsonWidgetsFull.text()).widgets;
        this.widgets.dispatchEvent(new CustomEvent("gotWidgetsList", { detail: this.baseWidgets }));
        this.widgets.baseWidgets = this.baseWidgets;
        this.widgets.activeWidgets = {};
        this.refreshWidgetsTimeout = setTimeout(this.getWidgets, REFRESH_WIDGETS_TIMEOUT * 2);
      } catch (ex) { console.error(ex); }
    }, 3000);
  }

  getWidgets = async () => {
    clearTimeout(this.refreshWidgetsTimeout);
    const { dumpObject, runJS } = this.context.stb;
    try {
        
        const visibility = await runJS(GET_WIDGETS_VISIBILITY);
        if (visibility && visibility !== this.visibility) {
            this.activeWidgets = await dumpObject(`${ROOT_PATH}.widgets`, 4);
            Object.keys(this.widgets).sort().forEach((key, index) => {
                set(this.widgets, `${key}.isVisible`, visibility[index] === "1")
            });
            this.widgets.activeWidgets = this.activeWidgets;
            const widgets = merge({}, this.activeWidgets, this.baseWidgets);
            this.widgets.dispatchEvent(new CustomEvent("changedWidgetsVisibility", { detail: widgets }));
            this.visibility = visibility;
        }
    } catch(ex) {
        console.error(ex);
    }
    this.refreshWidgetsTimeout = setTimeout(this.getWidgets, REFRESH_WIDGETS_TIMEOUT);
  }

  render() {
    return this.props.children;
  }
}

export default WidgetsManager;
