import React, { Component } from 'react';
import PropTypes from 'prop-types';
// import isEqual from "lodash/isEqual";
// import findIndex from "lodash/findIndex";
import "./DeviceScreen.css";
import { getImageData } from './utils';
import classNames from "classnames";
import ThemaVisualiser from './ThemaVisualiser';


const PREVIEW_WIDTH = 112; // 200;
const PREVIEW_HEIGHT = 63; // 112;
const PREVIEW_REFRESH_INTERVAL = 500;

const WIDTH = 600; // 720;
const HEIGHT = 335; // 405;
const REFRESH_INTERVAL = 900;
/*
function getPixelByIndex(imgData, index) {
  var i = index*4, d = imgData;
  return [d[i],d[i+1],d[i+2],d[i+3]] // returns array [R,G,B,A]
}

function getPixel(imgData, x, y) {
  return getPixelByIndex(imgData, y * imgData.width + x);
}

function getXYFromIndex(index) {
  return {
    x: Math.floor(index / 4) % PREVIEW_WIDTH,
    y: Math.floor((index / 4) / PREVIEW_WIDTH)
  }
} */

class DeviceScreen extends Component {
  static contextTypes = {
    stb: PropTypes.object
  };

  interval = null;
  previewInterval = null;
  canvasRef = null;
  imageRef = null;
  context2d = null;
  previousPreviewData = null;
  state = {
    isInputFocused: false,
    isCtrlDown: false
  }

  componentDidMount() {
    this.interval = setInterval(this.refreshImage, REFRESH_INTERVAL);
    // this.previewInterval = setTimeout(this.refreshPreview, PREVIEW_REFRESH_INTERVAL);
    this.refreshImage();
    document.addEventListener("keypress", this.handleDocumentKey);
  }

  componentWillUnmount() {
    clearInterval(this.interval);
    clearInterval(this.previewInterval);
    document.removeEventListener("keypress", this.handleDocumentKey);
  }

  setCanvasRef = ref => {
    this.canvasRef = ref;
    this.context2d = ref && ref.getContext("2d");
  };

  setImageRef = ref => { this.imageRef = ref; }

  refreshImage = () => {
    if (!this.imageRef) {
      return;
    }

    const { stb } = this.context;
    const url = stb.getScreenUrl(WIDTH, HEIGHT);
    // this.imageRef.src = url;

    const img = new Image();
    img.onload = () => {
      this.context2d && this.context2d.clearRect(0, 0, PREVIEW_WIDTH, PREVIEW_HEIGHT); // this.context2d.drawImage(img, 0, 0);
      this.imageRef.src = url;
    }
    img.src = url;
  }

  handleKeyDown = (e) => {
    const { keyCode, charCode, ctrlKey } = e;
    const { sendKeyboardKey, sendKey, reloadApp } = this.context.stb;
    // do nothing if not key code or char to send
    if (!keyCode && !charCode) return false;

    if (keyCode === 17) {
      return !this.state.isCtrlDown && this.setState({ isCtrlDown: true });
    }

    const ifKeyIsF5 = keyCode === 116;
    const ifKeyIsR = keyCode === 82;
    const ifKeyIsHome = keyCode === 36;

    if (ctrlKey || ifKeyIsHome || ifKeyIsF5) {
      const keyMappings = {
        49: "red",
        50: "green",
        51: "yellow",
        52: "blue",
        8: "back",
        122: "fullscreen",
        36: "menu"
      };

      const action = keyMappings[keyCode];
      if (action) {
        sendKey(action);
      }

      if (ifKeyIsR || ifKeyIsF5) {
        reloadApp();
      }
    } else {
      sendKeyboardKey(keyCode, charCode);
    }

    e.preventDefault();
  };

  handleButtonClick = ({ target }) => {
    const key = target.getAttribute("data-key");

    if (key) {
      this.context.stb.sendKey(key);
      this.inputRef.focus();
    }
  }

  handleDocumentKey = (e) => {
    const ifKeyIsF5 = e.keyCode === 116;
    const ifKeyIsCtrlR = e.ctrlKey && e.keyCode === 82;
    if (ifKeyIsCtrlR && ifKeyIsF5) {
      e.preventDefault();
      this.context.stb.reloadApp();

      return true;
    }
  }

  render() {
    const { stb } = this.context;
    const { isCtrlDown, isInputFocused } = this.state;
    const url = stb.getScreenUrl(WIDTH, HEIGHT);
    const classes = classNames(
      "DeviceScreen__keys Keys",
      isCtrlDown && "Keys--ctrlDown",
      isInputFocused && "Keys--inputFocused"
    );

    return (
      <div
        className="DeviceScreen"
        style={{width: WIDTH * 1.5, height: HEIGHT * 1.5}}
      >
        <input
          className="DeviceScreen__keyGrabber"
          onKeyDown={this.handleKeyDown}
          onKeyUp={() => isCtrlDown && this.setState({ isCtrlDown: false })}
          ref={input => this.inputRef = input}
          onFocus={() => this.setState({ isInputFocused: true })}
          onBlur={() => this.setState({ isInputFocused: false })}
        />
        <canvas
          width={PREVIEW_WIDTH}
          height={PREVIEW_HEIGHT}
          ref={this.setCanvasRef}
          src={url}
        />
        <img
          width={WIDTH}
          height={HEIGHT}
          ref={this.setImageRef}
          src={url}
        />
        {/*<ThemaVisualiser />*/}
        <div className={classes}>
          <span>All your keyboard input is now redirected to the device</span>
          <div>
            <button className="Keys__red" onClick={this.handleButtonClick} data-key="red">
              <span>CTRL + 1</span>
            </button>
            <button className="Keys__green" onClick={this.handleButtonClick} data-key="green">
              <span>CTRL + 2</span>
            </button>
            <button className="Keys__yellow" onClick={this.handleButtonClick} data-key="yellow">
              <span>CTRL + 3</span>
            </button>
            <button className="Keys__blue" onClick={this.handleButtonClick} data-key="blue">
              <span>CTRL + 4</span>
            </button>
            <button className="Keys__button" onClick={this.handleButtonClick} data-key="back">
              <img src="/images/back.png" />
              <span>CTRL + Backspace</span>
            </button>
            {/*<button className="Keys__button" onClick={this.handleButtonClick} key="ok">
              <img src="/images/ok.png" />
            </button> */}
            <button className="Keys__button" onClick={this.handleButtonClick} data-key="menu">
              <img src="/images/menu.png" />
              <span>Home</span>
            </button>
            <button className="Keys__button" onClick={this.handleButtonClick} data-key="fullscreen">
              <img src="/images/fullscreen.png" />
              <span>CTRL + F11</span>
            </button>
          </div>
        </div>
      </div>
    );
  }
}

export default DeviceScreen;
