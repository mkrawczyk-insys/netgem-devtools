export const dumpValue = ({ type, value, str, noData }) => {
    switch (type) {
        case 'undefined':
            return undefined;
        case 'coreObject':
        case 'json':
            if (noData) {
                return null;
            }

            return Object.keys(value).reduce((result, key) => {
                result[key] = dumpValue(value[key]);
                return result;
            }, {});
        case 'string':
            return value;
        case 'function':
        default:
            return str || value;
    }
}