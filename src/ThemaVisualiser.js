import React, { Component } from 'react';
import PropTypes from 'prop-types';
import "./ThemaVisualiser.css";
import set from "lodash/set";
import get from "lodash/get";
import merge from "lodash/merge";
import { decodeServerJson } from './utils';

const SCREEN_WIDTH = 1280;
const SCREEN_HEIGHT = 720;

const getPosition = ({ width, height, x, y, param = {} }) => ({
    width: parseInt(width || param.w || get(param, 'bounds.w', 0)),
    height: parseInt(height || param.h || get(param, 'bounds.h', 0)),
    top: parseInt(y || param.y || get(param, 'bounds.y', 0)),
    left: parseInt(x || param.x || get(param, 'bounds.x', 0))
});

class ThemaVisualiser extends Component {
  static contextTypes = {
    widgets: PropTypes.object
  };

  state = {
      widgets: {},
      activeElement: null
  };

  componentDidMount() {
    const { widgets } = this.context;
    widgets.addEventListener("gotWidgetsList", this.handleNewData);
    widgets.addEventListener("changedWidgetsVisibility", this.handleNewData);
    widgets.addEventListener("changedActiveElement", this.handleActiveElement);
  }

  componentWillUnmount() {
    const { widgets } = this.context;
    widgets.removeEventListener("gotWidgetsList", this.handleNewData);
    widgets.removeEventListener("changedWidgetsVisibility", this.handleNewData);
    widgets.removeEventListener("changedActiveElement", this.handleActiveElement);
  }

  handleNewData = ({ detail }) => this.setState({ widgets: detail });

  handleActiveElement = ({ detail }) => this.setState({ activeElement: detail });

  renderWidget = ({create, zIndex, isVisible, ...widgetData} = {}, name) => {
    if (this.state.activeElement !== name) { // (!isVisible) {
        return null;
    }

    const { width, height, top, left } = getPosition(widgetData);

    console.log(name, widgetData);
    const style = {
        width: `${width / SCREEN_WIDTH * 100}%`,
        height: `${height / SCREEN_HEIGHT * 100}%`,
        top: `${top / SCREEN_HEIGHT * 100}%`,
        left: `${left / SCREEN_WIDTH * 100}%`,
        zIndex
    }

    return (
        <div
            className="ThemaVisualiser__widget"
            key={name}
            style={style}
        >
            <label>{name || create}</label>
            <span>{width}px x {height}px</span>
        </div>
    )
  }

  render() {
    const { widgets, activeElement } = this.state;

    return (
        <div className="ThemaVisualiser">
            { activeElement &&
                Object.keys(widgets).map(key =>
                    widgets[key] && this.renderWidget(widgets[key], key)
                ).filter(Boolean)
            }
        </div>
    );
  }
}

export default ThemaVisualiser;
