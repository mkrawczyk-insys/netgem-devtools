import React, { Component } from 'react';
import STB from './STB';
import PropTypes from 'prop-types';
import last from "lodash/last";
import { parseStbCommandLog } from './utils';

class DeviceInnerError extends Error {
  constructor({ error, callstack, from, name }) {
    super(error);
    this.stack = from ? `${from}\n${callstack}` : callstack;
    this.from = from;

    const errorName = name || (error.match(/^(Catch|Uncaught)\s(\w+):/) || [])[2];

    if (errorName && !name) {
      this.name = errorName;
      const messageErrorIndex = error.indexOf(":") + 2;
      this.message = error.substring(messageErrorIndex);
    } else {
      this.name = name || "Error";
      this.message = error;
    }
  }
};

const FETCH_LOGS_TIMEOUT = 5000;

class Device extends Component {
  static contextTypes = {
    appData: PropTypes.object
  };

  static childContextTypes = {
    stb: PropTypes.object,
    printCommandLog: PropTypes.func
  };

  stb = null;
  loggedErrors = [];
  fetchLogsInterval = null;
  startDate = new Date().getTime();

  constructor(props) {
    super(props);
    this.stb = new STB(props.deviceIp, props.devicePort);
    this.stb.resetJavaScriptLogs();
    window.stb = this.stb;
  }

  async componentWillMount() {
    const { appData } = this.context;
    const response = await this.stb.runOverlayApp(appData.name, `http://${appData.ip}:${appData.port}`); 
    const currentApp = last(await response.json());
    if (currentApp && currentApp.id) {
      this.stb.setAppId(currentApp.id);
      console.info(`[STB]Current application instance: ${currentApp.id}`);
      this.getAndPrintLogs();
    }
  }

  getChildContext() {
    const { stb, printCommandLog } = this;

    return { stb, printCommandLog };
  }

  printSingleLog = (log) => {
    const { date } = log;
    const id = `${log.id}-${date}`;

    if (this.loggedErrors.includes(id)) {
      return;
    }

    this.loggedErrors.push(id);

    if (date < this.startDate) {
      // return;
    }

    const error = new DeviceInnerError(log);
    const [matchOutput,,type,value] = error.message.match(/^\[IO(:(\w*))?](.*)/) || [null, , "log"];
    if (matchOutput) {
      const dataToPrint = JSON.parse(value);

      switch (type) {
        case 'error': return console.error("[IO]", dataToPrint);
        case 'warn': return console.warn("[IO]", dataToPrint);
        case 'info': return console.info("[IO]", dataToPrint);
        default: return console.log("[IO]", dataToPrint);
      }
    }
    console.error(`[STB]${error.name}: ${error.message}`, error);
  }

  printCommandLog = (text) => {
    if (!text) {
      return console.log("[IO]", undefined);
    }
    const data = parseStbCommandLog(text);
    // console.log(data);
    this.printSingleLog(data);
  }

  getAndPrintLogs = async () => {
    clearTimeout(this.fetchLogsInterval);

    const logs = await this.stb.getJavaScriptLogs();
    logs.forEach(this.printSingleLog);
    
    // this.fetchLogsInterval = setTimeout(this.getAndPrintLogs, FETCH_LOGS_TIMEOUT);

    if (logs.length > 0) {
      this.stb.resetJavaScriptLogs();
    }
  }

  render() {
    return this.props.children;
  }
}

export default Device;
