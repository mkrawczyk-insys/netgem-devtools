import React, { Component } from 'react';
import './Console.css';
import { Hook, Console as ConsoleRenderer, Decode } from 'console-feed'
import TextareaAutosize from 'react-textarea-autosize';
import PropTypes from 'prop-types';
import { decodeServerJson } from './utils';

const theme = {
  BASE_FONT_FAMILY: 'Menlo, monospace',
  BASE_FONT_SIZE: '11px',
  BASE_LINE_HEIGHT: '14px',

  BASE_BACKGROUND_COLOR: 'white',
  BASE_COLOR: 'black',

  OBJECT_NAME_COLOR: 'rgb(136, 19, 145)',
  OBJECT_VALUE_NULL_COLOR: 'rgb(128, 128, 128)',
  OBJECT_VALUE_UNDEFINED_COLOR: 'rgb(128, 128, 128)',
  OBJECT_VALUE_REGEXP_COLOR: 'rgb(196, 26, 22)',
  OBJECT_VALUE_STRING_COLOR: 'rgb(196, 26, 22)',
  OBJECT_VALUE_SYMBOL_COLOR: 'rgb(196, 26, 22)',
  OBJECT_VALUE_NUMBER_COLOR: 'rgb(28, 0, 207)',
  OBJECT_VALUE_BOOLEAN_COLOR: 'rgb(28, 0, 207)',
  OBJECT_VALUE_FUNCTION_KEYWORD_COLOR: 'rgb(170, 13, 145)',

  HTML_TAG_COLOR: 'rgb(168, 148, 166)',
  HTML_TAGNAME_COLOR: 'rgb(136, 18, 128)',
  HTML_TAGNAME_TEXT_TRANSFORM: 'lowercase',
  HTML_ATTRIBUTE_NAME_COLOR: 'rgb(153, 69, 0)',
  HTML_ATTRIBUTE_VALUE_COLOR: 'rgb(26, 26, 166)',
  HTML_COMMENT_COLOR: 'rgb(35, 110, 37)',
  HTML_DOCTYPE_COLOR: 'rgb(192, 192, 192)',

  ARROW_COLOR: '#6e6e6e',
  ARROW_MARGIN_RIGHT: 3,
  ARROW_FONT_SIZE: 12,

  TREENODE_FONT_FAMILY: 'Menlo, monospace',
  TREENODE_FONT_SIZE: '11px',
  TREENODE_LINE_HEIGHT: '14px',
  TREENODE_PADDING_LEFT: 12,

  TABLE_BORDER_COLOR: '#aaa',
  TABLE_TH_BACKGROUND_COLOR: '#eee',
  TABLE_TH_HOVER_COLOR: 'hsla(0, 0%, 90%, 1)',
  TABLE_SORT_ICON_COLOR: '#6e6e6e',
  TABLE_DATA_BACKGROUND_IMAGE: 'linear-gradient(to bottom, white, white 50%, rgb(234, 243, 255) 50%, rgb(234, 243, 255))',
  TABLE_DATA_BACKGROUND_SIZE: '128px 32px'
};

exports.default = theme;
class Console extends Component {
  static contextTypes = {
    stb: PropTypes.object,
    printCommandLog: PropTypes.func
  };

  state = {
    logs: []
  }

  inputRef = null;

  componentDidMount() {
    Hook(window.console, log => {
      const decoded = Decode(log);
      const dataString = decoded && (decoded.data[0] || '').toString();
      if (dataString) {
        let addToState = false;
        if (dataString.startsWith(`[STB]`)) {
          decoded.data[0] = dataString.substring(5);
          addToState = true;
        } else if (dataString.startsWith("[IO]")) {
          decoded.data.shift();
          addToState = true;
        }

        if (addToState) {
          this.setState(({ logs }) => ({ logs: [...logs, decoded] }));
        }
      }
    });

    if (this.inputRef) {
      this.inputRef.addEventListener('keypress', this.handleInputKey);
    }
    console.info(`[STB]Console initialized`);
  }

  componentWillUnmount() {
    if (this.inputRef) {
      this.inputRef.removeEventListener('keypress', this.handleInputKey);
    }
  }

  changeInputRef = ref => {
    this.inputRef = ref;
  }

  parseCommandResponse = ({ logs }) => {
    if (!logs) {
      console.log(`[STB][IO]`, undefined);
    }
  }

  handleInputKey = async (e) => {
    if (e.keyCode === 13 && !e.shiftKey) {
      e.preventDefault();
  
      const value = e.target.value;
      e.target.value = "";

      console.log(`[STB]${value}`);
      const { stb, printCommandLog } = this.context;
      const [, type, codeToInject] = value.match(/^\s*console\.(log|warn|info|error)\s*\(\s*(.*)\s*\)/) || [, "log", value];
      const injectedScript = `
        throw new Error("[IO:${type}]"+JSON.stringify(${codeToInject}))
      `
      const response = await stb.injectJS(injectedScript);
      const { logs } = decodeServerJson(await response.text());
      printCommandLog(logs);
    }
  }
  
  render() {
    return (
      <div className="Console">
        <ConsoleRenderer logs={this.state.logs} variant="dark" />
        <TextareaAutosize
          autoFocus
          placeholder='Provide command here'
          inputRef={this.changeInputRef}
          />
      </div>
    );
  }
}

export default Console;
