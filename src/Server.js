import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Socket from 'simple-websocket';

class Server extends Component {
  static childContextTypes = {
    appData: PropTypes.object
  };

  socket = null;
  appData = null;
  state = {
    isServerReady: false
  }

  constructor(props) {
    super(props);
  }

  componentWillMount() {
    this.socket = new Socket('ws://localhost:8072');
    this.socket.on('data', this.handleData);
  }

  handleData = (data) => {
    const { command, ip, port, name } = JSON.parse(data);
    switch (command) {
      case 'getinfo':
        this.appData = { ip, port, name};
        this.setState({ isServerReady: true }, () => {
          console.info(`[STB]${name} server is running on ${ip}:${port}`);
        });
        break;
    }
  }

  componentWillUnmount() {
    this.socket.destroy();
  }

  getChildContext() {
    const { appData } = this;

    return { appData };
  }

  render() {
    return this.state.isServerReady ? this.props.children : null;
  }
}

export default Server;
