import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classNames from "classnames";
import { Console as ConsoleRenderer, Decode } from 'console-feed'
// import Inspector from 'console-feed/node_modules/react-inspector';

class SidebarElementsTab extends Component {
  static contextTypes = {
    stb: PropTypes.object,
    appData: PropTypes.object,
    widgets: PropTypes.object
  };

  state = {
    currentElement: null
  }

  componentDidMount() {
    const { widgets } = this.context;
    widgets.addEventListener("gotWidgetsList", this.handleNewData);
    widgets.addEventListener("changedWidgetsVisibility", this.handleNewData);
  }

  componentWillUnmount() {
    const { widgets } = this.context;
    widgets.removeEventListener("gotWidgetsList", this.handleNewData);
    widgets.removeEventListener("changedWidgetsVisibility", this.handleNewData);
  }

  handleNewData = () => this.forceUpdate();

  render() {
    const { baseWidgets, activeWidgets } = this.context.widgets;
    const { currentElement } = this.state;
    const activeWidgetsNames = Object.keys(activeWidgets || {});

    const currentData = currentElement && baseWidgets[currentElement];

    const widgetsList = Object.keys(baseWidgets || {}).sort();
    return (
        <div className="Sidebar__inner">
            <ul className="Sidebar__Elements">
                { baseWidgets && widgetsList.map((key) => (
                    <li key={key} className={classNames(
                        "Element",
                        currentElement === key && "Element--current",
                        activeWidgetsNames.includes(key) && "Element--active",
                    )}>
                        <a
                            onClick={() => this.setState({ currentElement: currentElement === key ? null : key })}
                            onMouseEnter={() => this.context.widgets.changeActiveElement(key)}
                            onMouseLeave={() => this.context.widgets.changeActiveElement(null)}
                        >
                            { key }
                        </a>
                    </li>
                )) }
            </ul>
            { currentData &&
                <div className="Element__currentElement">
                    <h3>
                        { currentElement }
                        <span title={currentData.create}>{ (currentData.create || "").toUpperCase()[0] }</span>
                    </h3>
                    {/*<Inspector data={baseWidgets[currentElement]} theme="chromeDark"/>
                     JSON.stringify(baseWidgets[currentElement], null, "  ") */}
                </div>
            }
        </div>
    );
  }
}

export default SidebarElementsTab;
