import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import Device from './Device';
import MainView from './MainView';
import Server from './Server';
import Sidebar from './Sidebar';
import WidgetsManager from './WidgetsManager';

class App extends Component {
  render() {
    return (
      <div className="App">
        <Server>
          <Device
            deviceIp={process.env.REACT_APP_DEVICE_IP || "192.168.102.104"}
            devicePort={process.env.REACT_APP_DEVICE_PORT || "80"}
          >
            {/*<WidgetsManager>*/}
              <MainView />
              {/*<Sidebar />*/}
            {/*</WidgetsManager>*/}
          </Device>
        </Server>
      </div>
    );
  }
}

export default App;
