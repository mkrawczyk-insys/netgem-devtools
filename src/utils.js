export const objectToQueryString = function(obj) {
    var str = [];
    for (var p in obj)
      if (obj.hasOwnProperty(p)) {
        str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
      }
    return str.join("&");
}

const getParsedValueByKey = (key, value) => {
    const parsers = {
        Count: parseInt,
        Uptime: parseInt,
        Date: Date.parse
    };
    const parser = parsers[key];

    return parser ? parser(value) : value;
}

export const parseStbLogs = (rawLogs) => {
    

    const rawLogsEntries = rawLogs.split("\n\n").map(item => item.trim()).filter(item => item.match(/^\[https?:\/\//));
    return rawLogsEntries.map((data) => {
        const lines = data.split("\n");
        const id = lines.shift().match(/^\[(.*)\]/)[1];

        let callstackLines = [];
        let isParsingCallstack = false;

        return lines.reduce((result, line, index) => {
            const [,key, value] = line.match(/^(\w*)=(.*)/) || [,null,line];

            if (isParsingCallstack) {
                if (key || lines.length - 1 === index) {
                    isParsingCallstack = false;
                    result.callstack = callstackLines.join("\n");
                    callstackLines = [];
                } else {
                    callstackLines.push(line);
                    return result;
                }
            }
            
            if (key && key === "Callstack") {
                isParsingCallstack = true;
            } else if(key) {
                result[key.toLowerCase()] = getParsedValueByKey(key, value);
            }
            return result;
        }, { id });
    });
}

export const parseStbCommandLog = (rawLog) => {
    function utf8_to_str(a) {
        for(var i=0, s=''; i<a.length; i++) {
            var h = a[i].toString(16)
            if(h.length < 2) h = '0' + h
            s += '%' + h
        }
        return decodeURIComponent(s)
    }
    const lines = rawLog.split("\n").map(item => item.trim()).filter(item => !item.startsWith("---- appjob:"));
    const [, id, date] = lines.shift().match(/^Javascript \[(.*)\]: (.*)/) || [];
    const [,, error] = lines.shift().match(/(: )(.*)/) || [];
    const callstack = lines.map(line => (line.match(/(  .*)/) || [])[1]).filter(Boolean).join("\n")

    return {
        id,
        date: getParsedValueByKey("Date", date),
        count: 0,
        error,
        callstack
    }
}

export const decodeServerJson = function(string, secure){
	if (!string || typeof string !== 'string') return null;

	if (secure || JSON.secure){
		if (JSON.parse) return JSON.parse(string);
		if (!JSON.validate(string)) throw new Error('JSON could not decode the input; security is enabled and the value is not secure.');
	}

	return eval('(' + string + ')');
};